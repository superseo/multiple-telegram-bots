const TelegramBot = require('node-telegram-bot-api');
const tokens = ['token1','token2' /* массив токенов через запятую, на каждый из них будет создан свой бот */];

let bots = [];

let capitalizeFirstLetter = (string) => string.charAt(0).toUpperCase() + string.slice(1);

function* dialog() {
	let name = yield "Здравствуйте! Меня забыли наградить именем, а как зовут вас?";
	// убираем ведущие знаки пунктуации, оставляем только 
	// первую компоненту имени, пишем её с заглавной буквы
	name = capitalizeFirstLetter( name.replace(/.!/g,'') );
	let likes_python = yield* ask_yes_or_no(`Приятно познакомиться, ${name}. Вам нравится Питон?`);
	if (likes_python) {
		answer = yield* discuss_good_js(name);
	} else {
		answer = yield* discuss_bad_js(name);
	}
}

function* ask_yes_or_no(question){
	// Спросить вопрос и дождаться ответа, содержащего «да» или «нет».
	// Возвращает :Boolean
	let answer = yield question;
	while ( !answer.includes('да') && !answer.includes('нет') ) {
		answer = yield "Так да или нет?"
	}
	return answer.includes('да');
}

function* discuss_good_js(name) {
	let answer = yield `Мы с вами, ${name}, поразительно похожи! Что вам нравится в нём больше всего?`;
	let likes_article = yield* ask_yes_or_no("Ага. А как вам, кстати, статья на Хабре? Понравилась?");
	if (likes_article) {
		answer = yield "Чудно!"
	} else {
		answer = yield "Жалко."
	}
	return answer;
}

function* discuss_bad_js(name) {
	let answer = yield `Ай-яй-яй. ${name}, фу таким быть! Что именно вам так не нравится?`
	let likes_article = yield* ask_yes_or_no("Ваша позиция имеет право на существование. Статья на Хабре вам, надо полагать, тоже не понравилась?");
	if (likes_article) {
		answer = yield "Ну и ладно.";
	} else {
		answer = yield "Что «нет»? «Нет, не понравилась» или «нет, понравилась»?";
		answer = yield "Спокойно, это у меня юмор такой.";
	}
	return answer;
}

tokens.forEach((token, index) => {
	let bot = bots[index];
	bot = new TelegramBot(token, {polling: true});
	let generator = dialog();

	bot.onText(/\/help/, function (msg, match) {
		var fromId = msg.from.id;
		bot.sendMessage(fromId, '\/echo, \/start, \/weather');
	});

	bot.onText(/\/start/, function (msg, match) {
		var chatId = msg.chat.id;
		// начинаем диалог заново, перезапустив генератор
		generator = dialog();
		let answer = generator.next(msg.text);
		bot.sendMessage(chatId, answer.value);
	});

	// Простая команда без параметров.
	bot.on('message', function (msg, match) {
		// выход если это была команда боту
		if (msg.entities) return;
		var chatId = msg.chat.id;
		let answer = generator.next(msg.text);
		bot.sendMessage(chatId, answer.value);
	});
})
